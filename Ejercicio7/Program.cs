﻿using System;

namespace YANDRYBRIONES3A
{
	class ejercicio7
	{

		static void Main(string[] args)
		{
			double a;
			double b;
			double c;
			String continuar;
			a = 0;
			b = 0;
			c = 0;
			Console.WriteLine("Ingrese el primero numero");
			a = Double.Parse(Console.ReadLine());
			Console.WriteLine("Ingrese el segundo numero");
			b = Double.Parse(Console.ReadLine());
			Console.WriteLine("Ingrese el tercer numero");
			c = Double.Parse(Console.ReadLine());
			do
			{
				if (a == b && a == c)
				{
					Console.WriteLine("Los numero ingresado forman un Equilatero");
				}
				else
				{
					if (a == b && a != c && b != c)
					{
						Console.WriteLine("Los numeros ingresados forman un Isoceles");
					}
					else
					{
						Console.WriteLine("Los numeros ingresados forman un Escaleno");
					}
				}
				Console.WriteLine("Desea continuar S/N");
				Console.WriteLine("Digitar en mayuscula");
				continuar = Console.ReadLine();
				Console.WriteLine("\n");
				Console.Clear();
			} while (continuar != "N");
			Console.WriteLine("Gracias");
			Console.ReadKey();
		}

	}

}

